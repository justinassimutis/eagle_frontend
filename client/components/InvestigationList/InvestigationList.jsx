import React, { Component } from "react";
import xhr from "axios";
import routes from "../../constants/routes";
import Filter from "../Filter/Filter"

class InvestigationList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      "investigations": [],
      "filter": ''
    }
  }

  componentDidMount() {
    this._getInvestigations();
  }

  _getInvestigations() {
    //const path = routes.API_INVESTIGATIONS;
    const path = "/client/mockData/data.json";
    xhr.get(path).then(response => {
      this.setState(
        Object.assign({}, this.state, {
          "investigations": response.data
        })
      )}
    )
  }

  setFilter(filter) {
    this.setState(
      Object.assign({}, this.state, {
        "filter": filter
      })
    )
  }

  _renderInvestigations() {
    return this.state.investigations
      .filter(investigation => investigation.title.indexOf(this.state.filter) > -1)
      .map((investigation, idx) => {
        return (
          <li key={idx}>{investigation.title}</li>
        );
      })
  }

  render() {
    return (
      <section>
        <Filter callback={this.setFilter.bind(this)} />
        <header>
          <h3>Investigation Titles</h3>
        </header>
        <ul className="list">
          {this._renderInvestigations()}
        </ul>
      </section>
    );
  }
}

export default InvestigationList;