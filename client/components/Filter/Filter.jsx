import React, { Component } from "react";

class Filter extends Component {
  constructor(props) {
    super(props);
  }

  _onKeyUp() {
    const value = this.refs.filterInput.value;
    this.props.callback(value);
  }
  
  render () {
    return (
      <section>
        <label style={{"display": "block"}}>
          Please enter the filter
        </label>
        <input 
          type="text" 
          onKeyUp={this._onKeyUp.bind(this)} 
          ref="filterInput"/>
      </section>
    )
  }
}

export default Filter;