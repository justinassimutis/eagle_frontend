import React, { Component } from "react";
import InvestigationList from '../InvestigationList/InvestigationList';

class IndexComponent extends Component {
  render() {
    return (
      <main>
        <h2>Eagle Frontend API</h2>
        <InvestigationList />
      </main>
    );
  }
}

export default IndexComponent;
