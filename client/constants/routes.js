import config from "../config/config";

const {
	api_version,
	client_id,
	client_key
} = config;

const routes = {
	"API_INVESTIGATIONS": `https://staging.eagle-core.com/api/${api_version}/investigations?client_id=${client_id}&client_key=${client_key}`
};

export default routes;